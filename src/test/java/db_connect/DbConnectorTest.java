package db_connect;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class DbConnectorTest {
    private static DbConnector dbConnector;
    
    @BeforeAll
    static void init() {
        dbConnector = new DbConnector();
    }
    
    @Test
    void should_get_db_config_info() {
        DbConfig dbConfig = dbConnector.getDbConfig();
        
        Assertions.assertNotNull(dbConfig);
        Assertions.assertEquals("jdbc:mysql://localhost:3306/student_management_system", dbConfig.getDatabaseURL());
        Assertions.assertEquals("root", dbConfig.getUsername());
        Assertions.assertEquals("123456", dbConfig.getPassword());
    }
    
    @Test
    void should_get_connect() throws SQLException {
        Connection connection = dbConnector.createConnect();
        
        Assertions.assertNotNull(connection);
    }
}