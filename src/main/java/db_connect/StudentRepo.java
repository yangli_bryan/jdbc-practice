package db_connect;

import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        boolean result = false;
        String query = "INSERT INTO student_jdbc (name, age, sex, phone) VALUES (?, ?, ?, ?)";
        try {
            Connection conn = dbConnector.createConnect();

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setInt(2, student.getAge());
            preparedStatement.setString(3, student.getGender());
            preparedStatement.setString(4, student.getPhone());
            preparedStatement.execute();

            conn.close();

            result = true;
        } catch (SQLException e) {
            System.out.println("Operation failed.");
        }
        return result;
    }
    
    public List<Student> findStudents() {
        List<Student> studentList = new ArrayList<>();
        ResultSet rs = null;
        try {
            Connection conn = dbConnector.createConnect();
            Statement statement = conn.createStatement();
            rs = statement.executeQuery("SELECT * FROM student_jdbc");
            while(rs.next()) {
                studentList.add(new Student(rs.getInt("id"), rs.getString("name"),
                                            rs.getInt("age"), rs.getString("sex"),
                                            rs.getString("phone")));
            }
        } catch (SQLException e) {
            System.out.println("Have trouble retrieving data.");
        }
        return studentList;
    }
    
    public boolean updateStudent(Student student) {
        boolean result = false;
        String query = "UPDATE student_jdbc SET name = ?, age = ?, sex = ?, phone = ? WHERE id = ?";
        try {
            Connection conn = dbConnector.createConnect();

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setInt(2, student.getAge());
            preparedStatement.setString(3, student.getGender());
            preparedStatement.setString(4, student.getPhone());
            preparedStatement.setInt(5, student.getId());
            preparedStatement.execute();

            conn.close();
            result = true;
        } catch (SQLException e) {
            System.out.println("Updating student information failed.");
        }
        return result;
    }
    
    public boolean deleteStudent(Student student) {
        boolean result = false;
        String query = "DELETE FROM student_jdbc WHERE id = ?";
        try {
            Connection conn = dbConnector.createConnect();

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, student.getId());
            preparedStatement.execute();

            conn.close();
            result = true;
        } catch (SQLException e) {
            System.out.println("Deleting student information failed.");
        }
        return result;
    }
}
